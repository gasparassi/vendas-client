import AddressService from '../services/AddressService';

/**
 * @typedef {AddressController}
 */
export default class AddressController {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = AddressService.build();
    }

    getFullAddressFromApi(cep){
      return this.service.get( "/" + cep ).then(
            response => {
                return response.data;
            }
        );
    }
}
