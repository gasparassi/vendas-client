import ClientService from '../services/ClientService';
import ClientModel from '../models/ClientModel';

/**
 * @typedef {ClientControllers}
 */
export default class ClientControllers {

    /**
    * @type {null}
     */

    service = null;

    constructor(){
        this.service = ClientService.build();
    }

    clientModel(){
        return new ClientModel();
    }

    rules = {
    required: (value) => !!value || "Este campo é obrigatório",
  }


    register( client ) {
        return this.service.create(client).then(
            response => {
                return response;
            }
        );
    }

    getAll() {
        return this.service.search(null).then(
            response => {
                return response.rows;
            }
        );
    }

}
