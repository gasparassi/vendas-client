/**
 * Client Model
 */

export default class ClientModel {

  id = null;
  nome = "";
  logradouro = "";
  numero = "";
  complemento = "";
  bairro = "";
  localidade = "";
  uf = "";
  cep = "";
  ddd = "";
  ibge = "";

  constructor(nome, logradouro, numero, complemento, bairro, localidade, uf, cep, ddd, ibge) {
    this.nome = nome,
    this.logradouro = logradouro,
    this.numero = numero;
    this.complemento = complemento;
    this.bairro = bairro;
    this.localidade = localidade;
    this.uf = uf;
    this.cep = cep;
    this.ddd = ddd;
    this.ibge = ibge;
  }
}
