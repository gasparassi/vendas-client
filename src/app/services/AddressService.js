import Rest from './Rest';

/**
 * @typedef {AddressService}
 */
export default class AddressService extends Rest {

    /**
     * @type {String}
     */
    static resource = '/addresses';

}
