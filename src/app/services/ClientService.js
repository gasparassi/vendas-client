import Rest from './Rest'

/**
 * @typedef {ClientService}
 */
export default class ClientService extends Rest {
    /**
     * @type {String}
     */
    static resource = '/clients';

}
