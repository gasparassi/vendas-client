import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
	mode: 'hash',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '/',
			component: () => import('@/views/dashboard/Index'),
			children: [
				// Dashboard
				{
					name: 'dashboard',
					path: '',
					component: () => import('@/views/dashboard/Dashboard')
				},
				{
					name: 'clients-form',
					path: 'pages/client/register',
					component: () => import('@/views/pages/client/ClientForm'),
				},
				{
					name: 'clients',
					path: 'pages/client/index',
					component: () => import('@/views/pages/client/Clients'),
				},
			]
		}
	]
});

export default router;
